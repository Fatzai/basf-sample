# Docker Build
```
docker build -t basf-sample .
```

# Spin Up
```
docker run -p 8080:80 -v $(pwd):/var/www/html  basf-sample
```